<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\EmployeeSalary;
use App\SalaryStructure; 

/*use DB; */

class EmployeeController extends Controller
{

    public function processSalary(Request $request){
        //return $request->all(); 
        $file = $request->file('employees'); 
        \Excel::load($file, function($reader) {
            $reader->each(function($sheet) {
                $empSalary = new EmployeeSalary; 
                $empSalary->months = $sheet->months;
                $empSalary->year = $sheet->year; 
                $empSalary->user_id = $sheet->user_id;
                //dd($sheet); 
                $salaryDetails = SalaryStructure::where('user_id', $sheet->user_id)->first();
                   // dd($salaryDetails->basic);
                $empSalary->basic =  $salaryDetails->basic * $sheet->pay_days;                 
                $empSalary->hra =  $salaryDetails->hra * $sheet->pay_days;
                $empSalary->da =  $salaryDetails->da * $sheet->pay_days;
                $empSalary->save();
            
            });
        });
        return redirect('salaries');
    }
    

    public function salaryDetails(Request $request){

        $employess  = EmployeeSalary::select('salaries.*', 'users.name as user')->leftJoin('users', 'users.id', '=', 'salaries.user_id')->paginate(100); 
        return view('salaries', compact('employess')); 
    }
}
