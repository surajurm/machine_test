<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\User; 
use App\SalaryStructure; 
use App\EmployeeSalary;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for($i =1; $i <= 100; $i++)
        {
            for($j=1; $j <= 100; $j++){
                $user = new User; 
                $user->name = $faker->name; 
                $user->email = $faker->email; 
                $user->password = bcrypt('secret'); 
                $user->save();
                $empStructure = new SalaryStructure;
                $empStructure->basic = 10000; 
                $empStructure->hra = 5000; 
                $empStructure->da = 5000; 
                $empStructure->user_id = $user->id; 
                $empStructure->save(); 
            }

        }
    }
}
